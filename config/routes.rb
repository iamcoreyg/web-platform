Rails.application.routes.draw do
  root 'datasets#show'
  get 'top_urls' => 'datasets#top_urls'
  get 'top_referrers' => 'datasets#top_referrers'
end
