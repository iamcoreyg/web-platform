require 'test_helper'

class DatasetsControllerTest < ActionController::TestCase
  setup do
    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s

  end

  test "Should have (at least) 4 responses" do
    get :top_urls, :format => "json"

    body = JSON.parse(response.body)
    if body.length >=4
      assert_response :success
    end
  end

end
