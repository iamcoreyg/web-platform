# README #

### Getting Started ###
1. `bundle install`
2. `rake db:migrate`
3. `rake db:seed`
4. `rails s`

### Regenerate DB ###
To regenerate the db run `rake db:reset`

### Testing ###
Testing is done via `rake test test/controllers/datasets_controller_test.rb`

### API ###
1. `[GET] /top_urls`
2. `[GET] /top_referrers`

### Reports ####
Both reports are loaded on http://localhost:3000/