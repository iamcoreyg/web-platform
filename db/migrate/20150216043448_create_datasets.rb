class CreateDatasets < ActiveRecord::Migration
  def change
    create_table :datasets do |t|
      t.string :url
      t.string :referrer, null: true
      t.string :db_hash

      t.timestamps null: false
    end
  end
end
