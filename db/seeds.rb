# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

example_urls = ["http://apple.com",
        "https://apple.com",
        "https://www.apple.com",
        "http://developer.apple.com",
        "http://en.wikipedia.org",
        "http://opensource.org",
        "http://stackoverflow.com",
        "http://iamcoreyg.com",
        "http://fakeurl.org",
        "http://wonka.com",
        "http://m-audio.com",
        "http://thenews.im",
        "http://news.ycombinator.com",

    ]

example_referrers = ["http://apple.com", "https://apple.com", "https://www.apple.com", "http://developer.apple.com",  "http://wonka.com",
                     "http://m-audio.com",
                     "http://thenews.im", nil]

Dataset.delete_all

1000000.times do |id_num|
  db_url = example_urls.sample
  db_ref = example_referrers.sample
  db_time = Date.today - [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].sample
  db_hash = Digest::MD5.hexdigest({id: id_num, url: db_url, referrer: db_ref, created_at: db_time}.to_s)

  Dataset.create([
    { url: db_url, referrer: db_ref, db_hash: db_hash, created_at: db_time}
  ])

  puts id_num
end