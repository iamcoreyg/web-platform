class DatasetsController < ApplicationController
  include DatasetsHelper

  def show
  end

  def top_urls
    @today = Date.today
    @num_days = 5

    respond_to :json
  end

  def top_referrers
    @today = Date.today
    @num_days = 5

    respond_to :json
  end


end