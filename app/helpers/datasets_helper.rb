module DatasetsHelper
  def fetch_distinct_urls(day)
    Dataset.select('DISTINCT url').where(created_at: day.beginning_of_day..day.end_of_day)
  end

  def num_distinct_urls(day)
    fetch_distinct_urls(day).count
  end

  def get_url_hits(url, day)
    Dataset.where(url: url, created_at: day.beginning_of_day..day.end_of_day).count
  end

  def get_urls(index, day)
    urls = fetch_distinct_urls(day)
    hash = urls.map { |x| {url: x.url, visits: get_url_hits(x.url, day)} }
    hash = hash.sort_by {|h| h.values[1]}.reverse
    hash[index]
  end


  #check referrals
  def fetch_distinct_refs(day, url)
    Dataset.select('DISTINCT referrer').where(created_at: day.beginning_of_day..day.end_of_day, url: url)
  end

  def get_ref_hits(ref, url, day)
    Dataset.where(referrer: ref, url: url, created_at: day.beginning_of_day..day.end_of_day).count
  end

  def get_refs(y, url, day)
    refs = fetch_distinct_refs(day, url)
    hash = refs.map { |x| {referrer: x.referrer, visits: get_ref_hits(x.referrer, url, day)} }
    hash = hash.sort_by {|h| h.values[1]}.reverse
    hash[y]
  end

end