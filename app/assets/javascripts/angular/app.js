var App = angular.module('App', ['angular-loading-bar']);


App.controller('TopUrlCtrl', function($scope, $http) {
    $http.get('/top_urls.json')
        .then(function(res){
            $scope.topUrls = res.data
        });
});

App.controller('RefCtrl', function($scope, $http) {
    $http.get('/top_referrers.json')
        .then(function(res){
            $scope.topRefs = res.data
        });
});

App.filter('range', function() {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++)
            input.push(i);
        return input;
    };
})